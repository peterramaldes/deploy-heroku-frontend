export default {
    API_URL: 'https://peter-my-money-app-backend.herokuapp.com/api',
    OAPI_URL: 'https://peter-my-money-app-backend.herokuapp.com/oapi',
}