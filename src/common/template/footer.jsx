import React from 'react'

export default props => (
    <footer className='main-footer'>
        <strong>
            Copyright &copy; 2017
            <a href="https://github.com/peterramaldes" target='_blank'> Peter A. Ramaldes </a>
        </strong>
    </footer>
)